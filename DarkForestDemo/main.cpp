#include <iostream>
#include <application.h>

class testApp : public DoddzyCodes::DCEngine::Application
{
    public:
        testApp(int argc, const char** argv) : Application(argc, argv) {}
        bool startup() override { return true; }
        void shutdown() override { }

        void update() override {}
        void render() override {}
    private:
};


int main(int argc, const char** argv)
{
    testApp app(argc, argv);
    app.run(".config/game.conf");
    return 0;
}

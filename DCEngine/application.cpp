#include "application.h"
#include <cassert>
#include <easylogging++.h>
#include <iostream>
#include <json.hpp>
#include <opengl.h>

INITIALIZE_EASYLOGGINGPP

namespace DoddzyCodes {
namespace DCEngine {

inline MAKE_LOGGABLE(Application::AppInfo, appInfo, os)
{
    os << "Application Info: \n";
    os << "\tTitle:\t" << appInfo.windowTitle << "\n";
    os << "\tWidth:\t" << appInfo.width << "\n";
    os << "\tHeight:\t" << appInfo.height << "\n";
    os << "\tFullscreen:\t" << appInfo.isFullscreen << "\n";
    os << "\n";

    return os;
}

Application::Application(int argc, const char* argv[])
    : m_frameTime(m_appInfo.frameTime)
{
    el::Loggers::configureFromGlobal("./config/log.conf");
    START_EASYLOGGINGPP(argc, argv);

    el::Loggers::addFlag(el::LoggingFlag::ColoredTerminalOutput);
    el::Loggers::addFlag(el::LoggingFlag::ImmediateFlush);

    m_appInfo.argc = argc;
    m_appInfo.argv = argv;
}

int Application::run(std::string configFile)
{
    LOG(INFO) << "========================================";
    LOG(INFO) << "****************************************";
    LOG(INFO) << "========================================";
    LOG(INFO) << "Application Started..";
    readConfigFile(configFile);

    assert(initEngine());

    LOG(INFO) << "Running application startup";
    assert(startup());
    LOG(INFO) << "Completed application startup";

    runGameLoop();

    LOG(INFO) << "Running application shutdown";
    shutdown();
    LOG(INFO) << "Completed application shutdown";
    destroyEngine();

    LOG(INFO) << "Application Completed, have a nice day!";
    LOG(INFO) << "========================================";
    LOG(INFO) << "****************************************";
    LOG(INFO) << "========================================\n\n";
    return 0;
}

void Application::readConfigFile(std::string configFile)
{
    LOG(INFO) << "Loading config file: " << configFile;
    m_configurationFile = configFile;

    using json = nlohmann::json;

    std::ifstream configStream(configFile);
    LOG_IF(!configStream.is_open(), ERROR)
        << "Failed to load configuration file, using defaults";
    if (configStream.is_open())
    {
        json configJson;
        configStream >> configJson;
        if (configJson.find("windowTitle") != configJson.end())
        {
            m_appInfo.windowTitle =
                configJson["windowTitle"].get<std::string>();
        }
        else
        {
            LOG(ERROR)
                << "ERROR: Unable to load window title from configuration file";
        }
        if (configJson.find("width") != configJson.end())
        {
            m_appInfo.width = configJson["width"].get<int>();
        }
        else
        {
            LOG(ERROR)
                << "ERROR: Unable to load window width from configuration file";
        }
        if (configJson.find("height") != configJson.end())
        {
            m_appInfo.height = configJson["height"].get<int>();
        }
        else
        {
            LOG(ERROR) << "ERROR: Unable to load window height from "
                          "configuration file";
        }
        if (configJson.find("fullscreen") != configJson.end())
        {
            m_appInfo.isFullscreen = configJson["fullscreen"].get<bool>();
        }
        else
        {
            LOG(ERROR) << "ERROR: Unable to load window height from "
                          "configuration file";
        }
    }
}

bool Application::initEngine()
{
    LOG(INFO) << "Starting DCEngine";
    LOG(INFO) << "Initialising GLFW..";
    bool wasError = !glfwInit();
    LOG_IF(wasError, FATAL) << "Failed to initialise GLFW";
    assert(!wasError);

    LOG(INFO) << "Initialising window";
    LOG(INFO) << m_appInfo;

    m_appInfo.window = glfwCreateWindow(
        m_appInfo.width,
        m_appInfo.height,
        m_appInfo.windowTitle.c_str(),
        (m_appInfo.isFullscreen ? glfwGetPrimaryMonitor() : nullptr),
        nullptr);
    LOG_IF(!m_appInfo.window, FATAL) << "Failed to create window!";
    assert(m_appInfo.window);

    glfwMakeContextCurrent(m_appInfo.window);

    LOG(INFO) << "Loading OpenGL functions";
    wasError = !ogl_LoadFunctions();
    LOG_IF(wasError, FATAL) << "Unable to load OpenGL functions";

    LOG(INFO) << "Loaded OpenGL " << ogl_GetMajorVersion() << ", "
              << ogl_GetMinorVersion();

    LOG(INFO) << "DCEngine initialisation complete";
    return true;
}

void Application::destroyEngine()
{
    LOG(INFO) << "Shutting down DCEngine";
    glfwDestroyWindow(m_appInfo.window);
    glfwTerminate();
}

void Application::runGameLoop()
{
    m_shouldKeepRunning         = true;
    m_appInfo.appStartTime      = Clock::now();
    m_appInfo.previousFrameTime = Clock::now();
    while (m_shouldKeepRunning)
    {
        calculateTimings();

        update();
        render();

        glfwPollEvents();
        glfwSwapBuffers(m_appInfo.window);
    }
}

void Application::calculateTimings()
{
    auto now = Clock::now();
    m_frameTime =
        std::chrono::duration_cast<ms>(now - m_appInfo.previousFrameTime)
            .count();
    m_appInfo.previousFrameTime = now;
}

} // namespace DCEngine
} // namespace DoddzyCodes

#pragma once

#include <chrono>
#include <string>


struct GLFWwindow;

namespace DoddzyCodes {
namespace DCEngine {
class Application
{
    using Clock = std::chrono::high_resolution_clock;
    using ms    = std::chrono::duration<float, std::milli>;

  public:
    struct AppInfo
    {
        std::string windowTitle = "Unnamed Application";
        int width               = 1280;
        int height              = 720;
        bool isFullscreen       = false;

        GLFWwindow* window = nullptr;

        Clock::time_point appStartTime;
        Clock::time_point previousFrameTime;

        float frameTime = 0.0f;

        int argc;
        const char** argv;
    };

    Application() = delete;
    Application(int argc, const char* argv[]);
    virtual ~Application()              = default;
    Application(const Application& app) = delete;
    Application(Application&& app)      = default;
    Application& operator=(const Application& app) = delete;
    Application& operator=(Application&& app) = default;

    int run(std::string configFile); 

    const AppInfo& getAppInfo() const
    {
        return m_appInfo;
    }

    void setRunning(bool shouldRun)
    {
      m_shouldKeepRunning = shouldRun;
    }

  protected:
    virtual bool startup()  = 0;
    virtual void shutdown() = 0;
    virtual void update()   = 0;
    virtual void render()   = 0;

  private:
    void readConfigFile(std::string configFile);
    void calculateTimings();

    bool initEngine();
    void destroyEngine();

    void runGameLoop();
    std::string m_configurationFile;

    bool m_shouldKeepRunning = true;
    AppInfo m_appInfo;

    float& m_frameTime;
};
} // namespace DCEngine
} // namespace DoddzyCodes
